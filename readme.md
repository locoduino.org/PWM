# Bibliothèque de gestion des PWM

Copyright G. Marty 2015
Licence GPLv3
Fait pour locoduino.org

## Changer la fréquence

6 broches PWM pour un avr type uno pro mini etc...
Elles marchent par paire, ce fait est lié aux timers. Chaque broche possède une fréquence propre dont l'arduino utilise pour créer les PWM.
* 5 et 6 (timer 0) 980 Hz
* 9 et 10 (timer 1) 490 Hz
* 3 et 11 (timer 2) 490 Hz

Changer la fréquence sur l'une changera sur l'autre.

### Problèmes à noter
* si changement de fréquence sur les broches appartenant au timer 1, problème sur la librairie Servo
* si changement de fréquence sur les broches appartenant au timer 0 et 2, problème sur les fonctions delay() et millis()

### Comprendre la fonction
La fonction setPwmFrequency est à placer dans le setup.

Comme chaque fréquence propre à chaque broche, les paires de broche ont une fréquence de base bien à eux. Pour obtenir la fréquence que l'on veut, il faut connaître 2 choses la fréquence la plus haute et les diviseurs possibles : 
* 3, 9, 10 et 11 : fréquence haute de 31250 Hz
* 5 et 6 : fréquence haute de 62500 Hz

* 3 et 11, diviseurs possibles : 1, 8, 32, 64, 128, 256, et 1024
* 5, 6, 9 et 10, diviseurs possibles :  1, 8, 64, 256 et 1024

Les paramètres de la fonction setPwmFrequency sont au nombre de 2 :
* le premier accepte le numéro de broche, n'oublions pas que changer sur la 3 change la fréquence de la 11 et ainsi de suite. Ne mettre qu'une seule broche dans ce paramètre
* le deuxième accepte le diviseur correspondant à la broche que nous avons marqué auparavant comme 1er paramètre.

Pour obtenir la fréquence, il faut avoir la fréquence haute et le diviseur. Voir ci-après quelques exemples pour comprendre :

* Mettre la fréquence de la broche 9 à 3906 Hz (31250/8 = 3906) : setPwmFrequency(9, 8)
* Mettre la fréquence de la broche 6 à 62500 Hz (62500/1 = 62500) : setPwmFrequency(6, 1)
* Mettre la fréquence de la broche 10 à 31 Hz (31250/1024 = 31) : setPwmFrequency(10, 1024)